# Solar System
Solar System is a mod for [Besiege](https://store.steampowered.com/app/346010) which adds a Solar System level with realistic orbital mechanics, based on the main menu. It is also planned to hook into other mods (Air Density, Instrumentality, Camera Control Overhaul) for extra functionality.

## Usage
This mod has not been released yet.

1. Go to the main menu.
2. Click on the moon.

## Building
The project will compile with Visual Studio (2015 or 2019). <br>
Simply drop the repository into the Mods folder (Besiege/Besiege_data/Mods) and the game will recognise and load the mod.
