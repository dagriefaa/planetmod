﻿using UnityEngine;
using System.Collections;

/* Something to deal with z-fighting between emissives and planet surfaces at long ranges. Doesn't work too well.
 */
public class ProximityScale : MonoBehaviour {

    void Start() {
        maxScale = transform.localScale.x;
    }

    // Update is called once per frame
    void Update() {
        float distance = Vector3.Distance(Camera.main.transform.position, transform.position);
        float scale = maxScale;
        if (distance > minRange && distance < maxRange) {
            float fraction = (distance - minRange) / maxRange;
            scale = minScale + ((maxScale - minScale) * fraction);
        } else if (distance > maxRange) {
            scale = minScale;
        }
        transform.localScale = Vector3.one * scale;
    }

    public float maxScale;
    float minScale = 0.6f;

    float minRange = 6000f;
    float maxRange = 12000f;
}
