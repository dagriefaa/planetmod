﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

/* Stripped-down StartGameButton which opens Barren Expanse and activates solar system.
 */
public class SolarSystemButton : MonoBehaviour {

    // Token: 0x06001BF3 RID: 7155 RVA: 0x0009F0B4 File Offset: 0x0009D2B4
    private void OnMouseUp() {
        base.StartCoroutine(this.IELoadLevel());
    }

    // Token: 0x06001BF5 RID: 7157 RVA: 0x0009F0FC File Offset: 0x0009D2FC
    public IEnumerator IELoadLevel() {
        if (this.FadeCodey != null) {
            yield return this.FadeCodey.StartCoroutine("FadeIn");
        }
        PlanetController.LoadingSolarSystem = true;
        SceneManager.LoadScene(6, LoadSceneMode.Single); // 6 is Barren Expanse
        yield break;
    }

    // Token: 0x040018D2 RID: 6354
    public FadeScreen FadeCodey;
}
