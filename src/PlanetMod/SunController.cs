﻿using UnityEngine;
using System.Collections;

/* Controller for solar sun rotation, colour, flares, etc.
 */
public class SunController : MonoBehaviour {

    public bool Rotation = true; // sun movement?

    // seconds for sun to make a full orbit
    float _rotationPeriod = 3600f;
    public float RotationPeriod { get { return _rotationPeriod; } set { _rotationPeriod = value;
            if (rotation) rotation.degreesPerSecond = 360f / RotationPeriod;
        } }

    Rotate rotation = null;
    public static MeshRenderer renderer { get; private set; } = null;

    void Start() {
        rotation = GetComponent<Rotate>();
        renderer = GetComponent<MeshRenderer>();
    }
    
    void FixedUpdate() {
        if (!initialised) {
            if (!MainLightController.SetFlare("50mmZoom")) return;
            MainLightController.Color = new Color(1f, 0.95f, 0.9f);
            MainLightController.Intensity = 1.2f;
            MainLightController.Rotation = new Vector3(0f, Random.value * 360f, 0f); // one angle is boring
            MainLightController.AmbientColor *= 0.5f;
            initialised = true;
        }
        rotation.enabled = Rotation;
        if (Rotation) {
            MainLightController.Rotation += new Vector3(0f, RotationPeriod / 3600f * Time.fixedDeltaTime, 0f);
        }
    }

    bool initialised = false;
}
