﻿using System;
using UnityEngine;
using UnityEngine.Rendering;
using Modding.Blocks;
using Modding;
using System.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using spaar.ModLoader.Internal.Tools;

// credits to TesseractCat for the original Moon Mod, which this mod is based on

public class PlanetController : MonoBehaviour {

    /* todo:
     * orient camera with SoI
     * shift planets to different layer for rendering large distances
     * actual atmosphere shader
     */

    static readonly float Scale = 3f;

    // Token: 0x0600000A RID: 10 RVA: 0x00002208 File Offset: 0x00000408
    private void LevelLoaded(Scene s, LoadSceneMode lsm) {

        if (s.buildIndex == 1) { // main menu
            // swap the old and new moons because i need a working tooltip
            Transform moon = GameObject.Find("Planet").transform.FindChild("Moon/Moon");
            Transform newMoon = GameObject.Find("Orbit").transform.FindChild("Items/Moon")?.transform;
            moon.transform.parent = GameObject.Find("Orbit").transform.FindChild("Items").transform;
            moon.transform.localPosition = newMoon.transform.localPosition;
            newMoon.transform.parent = GameObject.Find("Planet").transform.FindChild("Moon");
            newMoon.transform.localPosition = moon.transform.localPosition;
            moon.gameObject.SetActive(true);
            
            moon.GetComponent<HighlightOnMouseOver>().colourToLerpTo = GameObject.Find("Planet").transform.FindChild("Moon/SANDBOX").gameObject.GetComponent<HighlightOnMouseOver>().colourToLerpTo;
            moon.GetComponent<HighlightOnMouseOver>().highlightMaterial = GameObject.Find("Planet").transform.FindChild("Moon/SANDBOX").gameObject.GetComponent<HighlightOnMouseOver>().highlightMaterial;
            
            // set up tooltip
            Tooltip t = moon.GetComponent<Tooltip>();
            t.tooltipParent.gameObject.SetActive(true);
            t.tooltipParent.FindChild("IPSILON king").GetComponent<TextMesh>().color = Color.white;
            t.tooltipParent.FindChild("IPSILON king").GetComponent<TextMesh>().text = "Planet System";
            t.tooltipParent.FindChild("Lock A").gameObject.SetActive(false);
            t.tooltipParent.GetChild(t.tooltipParent.FindChild("Lock A").GetSiblingIndex() + 1).gameObject.SetActive(false);
            t.tooltipParent.GetChild(t.tooltipParent.FindChild("Lock A").GetSiblingIndex() + 2).gameObject.SetActive(false);
            
            // grab a button with working components
            GameObject sandboxButton = GameObject.Find("Orbit").transform.FindChild("Items/SANDBOX").gameObject;

            // set up button components
            SolarSystemButton ssb = moon.gameObject.AddComponent<SolarSystemButton>(); // special button with special flag
            ssb.FadeCodey = sandboxButton.GetComponent<StartGameButton>().FadeCodey;
            
            ScreenSpaceParticleOnClick ssp = moon.gameObject.AddComponent<ScreenSpaceParticleOnClick>();
            ScreenSpaceParticleOnClick sandboxSsp = sandboxButton.GetComponent<ScreenSpaceParticleOnClick>();
            ssp.cam = sandboxSsp.cam;
            ssp.particleSys = sandboxSsp.particleSys;
            ssp.particleTrans = sandboxSsp.particleTrans;
            ssp.zPos = sandboxSsp.zPos;
            
            SoundOnMouseEvents sme = moon.gameObject.AddComponent<SoundOnMouseEvents>();
            SoundOnMouseEvents sandboxSme = sandboxButton.GetComponent<SoundOnMouseEvents>();
            sme.mouseDownSFX = sandboxSme.mouseDownSFX;
            sme.mouseOverSFX = sandboxSme.mouseOverSFX;
            sme.mouseUpSFX = sandboxSme.mouseUpSFX;
        }

        Events.OnBlockInit -= WaterCannonFix;

        if (!LoadingSolarSystem) { // not solar system map
            Physics.gravity = this.grav;
            SolarSystemLoaded = false;
            PlanetSystem?.SetActive(false); // may execute before planet is generated
        } else { // is solar system map
            // clean up junk
            Destroy(GameObject.Find("STATIC"));
            Destroy(GameObject.Find("TUTORIAL"));
            Destroy(GameObject.Find("ICE FREEZE"));
            Destroy(GameObject.Find("WORLD BOUNDARIES_LARGE"));
            Destroy(GameObject.Find("FloorBig"));
            Destroy(GameObject.Find("FOG SPHERE"));

            PlanetSystem.SetActive(true);
            GameObject.Find("PHYSICS GOAL")?.transform.Translate(new Vector3(0f, -500f));
            GameObject.Find("ATMOSPHERE")?.transform.Translate(new Vector3(0f, -500f));
            GameObject starSphere = GameObject.Find("AviamisAtmosphere").transform.FindChild("STAR SPHERE").gameObject;
            starSphere.transform.localScale = Vector3.one * 70000;
            starSphere.AddComponent<SunController>();
            starSphere.transform.FindChild("StarField (1)").gameObject.SetActive(false);
            starSphere.SetActive(true);

            MouseOrbit.maxZoom = 33500f;

            FindObjectOfType<WinCondition>().sandBoxLevel = true;
            Events.OnBlockInit += WaterCannonFix;

            LoadingSolarSystem = false;
            SolarSystemLoaded = true;
        }
    }

    // Token: 0x0600000B RID: 11 RVA: 0x000026BB File Offset: 0x000008BB
    private void Start() {
        this.grav = Physics.gravity;
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += this.LevelLoaded;
        LevelLoaded(SceneManager.GetActiveScene(), LoadSceneMode.Single);

        ObjectExplorer.Storage["AirDensity"] = new List<object> {
            4500f * Scale,
            new List<object[]>()
        };

        List<GradientColorKey> atmoColours = new List<GradientColorKey> {
            new GradientColorKey(new Color(0.58f, 0.65f, 0.71f), 0f),
            new GradientColorKey(new Color(0.58f, 0.65f, 0.71f), 0.25f),
            new GradientColorKey(Color.black, 0.75f),
            new GradientColorKey(Color.black, 1f)
        };
        atmosphericColour.colorKeys = atmoColours.ToArray();

        PlanetSystem = (GameObject)Instantiate(GameObject.Find("Planet"), worldCenter, Quaternion.identity);
        DontDestroyOnLoad(PlanetSystem);
        PlanetSystem.SetLayerRecursively(29); // floor layer
        PlanetSystem.transform.FindChild("PLANET").localPosition = Vector3.zero;
        PlanetSystem.transform.FindChild("PLANET").localEulerAngles = new Vector3(0, 0, 110f);
        SetupGravityWell(PlanetSystem.transform.FindChild("PLANET").gameObject, 4500f * Scale, false, 4500f * Scale, 1000f);

        // planet object setup
        PlanetSystem.transform.FindChild("PLANET/Water").localScale *= 1.05f;
        CrumpleMesh cm = PlanetSystem.transform.FindChild("PLANET/Water").GetComponent<CrumpleMesh>();
        cm.amount *= 0.1f;
        PlanetSystem.GetComponentsInChildren<Collider>().ToList().ForEach(x => x.isTrigger = false);
        Destroy(PlanetSystem.GetComponent<Rigidbody>());
        PlanetSystem.GetComponentsInChildren<IslandConqueredFX>().ToList().ForEach(x => {
            x.objDisableOnConquer?.gameObject?.SetActive(true);
            x.objEnableOnConquer?.gameObject?.SetActive(false);
            x.islandVis.material = x.islandNormalMaterial;
            Destroy(x);
        });

        Transform islands = PlanetSystem.transform.FindChild("PLANET/ISLANDS");
        islands.localEulerAngles += new Vector3(-30f, 0f, 0f);

        // colliders for terrain
        islands.FindChild("IPSILON").GetComponent<MeshCollider>().convex = false;
        islands.FindChild("IPSILON/IPSILON Vis/LyreStatue")?.gameObject.SetActive(false);
        islands.FindChild("IPSILON/IPSILON Vis/Cottage")?.gameObject.SetActive(false);
        islands.FindChild("TOLBRYND").GetComponent<MeshCollider>().convex = false;
        islands.FindChild("TOLBRYND/Empty/Sword1")?.gameObject.SetActive(false);
        islands.FindChild("TOLBRYND/Empty/PlanetpropsWindmill")?.gameObject.SetActive(false);
        islands.FindChild("TOLBRYND/Empty/PlanetpropsTolbryndHouse")?.gameObject.SetActive(false);
        islands.FindChild("VALFROSS").GetComponent<MeshCollider>().convex = false;
        islands.FindChild("VALFROSS/Valfross/default")?.gameObject.SetActive(false);
        islands.FindChild("KROLMAR/Col").GetComponent<MeshCollider>().convex = false;
        islands.FindChild("KROLMAR/Default/GameObject")?.gameObject.SetActive(false);
        foreach (Transform t in PlanetSystem.transform.FindChild("PLANET/TREES")) {
            DestroyImmediate(t.gameObject.GetComponent<Rigidbody>());
            t.gameObject.AddComponent<MeshCollider>();
        }
        foreach (Transform t in islands.FindChild("Mountains")) {
            DestroyImmediate(t.gameObject.GetComponent<Rigidbody>());
            t.gameObject.AddComponent<MeshCollider>();
        }

        Transform orbit = PlanetSystem.transform.FindChild("Moon");
        orbit.gameObject.SetActive(true);

        orbit.GetComponent<Rotate>().enabled = false;

        PlanetSystem.transform.localScale = Vector3.one * 3480f * Scale;
        foreach (Transform t in orbit) {
            t.localPosition *= 4.5f; //  e x p a n d
        }

        Transform moon = orbit.FindChild("Moon");
        moon.gameObject.SetActive(true);
        Destroy(moon.GetComponent<SphereCollider>());
        moon.gameObject.AddComponent<MeshCollider>().convex = true;
        moon.GetComponent<Renderer>().material.color = Color.grey;
        SetupGravityWell(moon.gameObject, 350f * Scale, true, 0f, 0f);
        
        orbit.FindChild("BARREN EXPANSE").GetChild(0).gameObject.AddComponent<ProximityScale>();
        SetupGravityWell(orbit.FindChild("BARREN EXPANSE").gameObject, 300f * Scale, true, 0f, 0f);
        orbit.FindChild("SANDBOX/Blue").gameObject.AddComponent<ProximityScale>();
        orbit.FindChild("SANDBOX/Mesh").gameObject.AddComponent<MeshCollider>().convex = true;
        SetupGravityWell(orbit.FindChild("SANDBOX").gameObject, 400f * Scale, true, 0f, 0f);

        Destroy(orbit.FindChild("black hole 3").gameObject);
        Destroy(orbit.FindChild("black hole 2").gameObject);

        // clean up functional menu components
        PlanetSystem.GetComponentsInChildren<HighlightOnMouseOver>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<ScreenSpaceParticleOnClick>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<PlanetRotateMouse>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<IslandController>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<SoundOnMouseEvents>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<Tooltip>().ToList().ForEach(x => x.enabled = false);
        PlanetSystem.GetComponentsInChildren<AudioSource>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<Rotate>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<AudioSource>().ToList().ForEach(x => Destroy(x));
        PlanetSystem.GetComponentsInChildren<StartGameButton>().ToList().ForEach(x => Destroy(x));
        
        PlanetSystem.SetActive(false);
        Debug.Log("[PlanetMod] Setup complete.");
    }

    // Token: 0x0600000C RID: 12 RVA: 0x000026E0 File Offset: 0x000008E0
    private void Update() {
        if (SolarSystemLoaded) {
            // remove coloured fog, the hard way
            if (!fogg) {
                fogg = Camera.main.gameObject.GetComponent<ColorfulFog>();
                if (fogg) fogg.enabled = false;

                Camera mainCamera = Camera.main;

                Camera planetCamera = (Instantiate(mainCamera.gameObject, mainCamera.transform, true) as GameObject).GetComponent<Camera>();
                planetCamera.gameObject.name = "Planet Camera";
                planetCamera.nearClipPlane = 4000f;
                planetCamera.farClipPlane = 150000f;
                planetCamera.clearFlags = CameraClearFlags.Color;
                planetCamera.backgroundColor = Color.black;
                planetCamera.depth = -2;
                DestroyImmediate(planetCamera.gameObject.GetComponent<MouseOrbit>());
                DestroyImmediate(planetCamera.gameObject.GetComponent<Vignetting>());
                planetCamera.gameObject.GetComponent<ColorCorrectionLut>().enabled = false;
                DestroyImmediate(planetCamera.gameObject.GetComponent<cakeslice.OutlineEffect>());
                foreach (Transform t in planetCamera.transform) {
                    Destroy(t.gameObject);
                }

                mainCamera.farClipPlane = 4000f;
                mainCamera.clearFlags = CameraClearFlags.Depth;

            }

            // atmosphere colour
            Vector3 worldCenterToCamera = Camera.main.transform.position - worldCenter;
            float atmosphereScattering = Mathf.Abs(1f - (Vector3.Angle(worldCenterToCamera, Quaternion.Euler(MainLightController.Rotation) * Vector3.forward) / 180f))
                    + Mathf.Clamp((worldCenterToCamera.magnitude - 1750f) / (2500f - 1750f), 0f, 1f);
            Camera.main.backgroundColor = atmosphericColour.Evaluate(atmosphereScattering);
            Color starColour = SunController.renderer.materials[0].GetColor("_TintColor");
            starColour.a = (atmosphereScattering < 0.5f) ? 0f : ((atmosphereScattering - 0.5f) * 2f);
            SunController.renderer.materials[0].SetColor("_TintColor", starColour);
        }
    }

    void FixedUpdate() {
        // it keeps getting reset
        if (SolarSystemLoaded) {
            Physics.gravity = Vector3.zero;
        }
    }

    private void WaterCannonFix(Modding.Blocks.Block obj) {
        if (!(obj.InternalObject is WaterCannonController)) return;

        ParticleSystem.InheritVelocityModule inheritVelocityModule;

        WaterCannonController wc = obj.InternalObject as WaterCannonController;
        foreach (ParticleSystem p in wc.waterParticles) {
            if (p.gameObject.GetComponent<ParticleDragWithSpeed>()) return;

            inheritVelocityModule = p.inheritVelocity;
            inheritVelocityModule.enabled = true;

            //ParticleDragWithSpeed pd = p.gameObject.AddComponent<ParticleDragWithSpeed>();
            //pd.myRigidbody = wc.Rigidbody;
            //pd.waterCannon = wc;

            UnityEngine.Object.Destroy(p.gameObject.GetComponent<ParticleAddForce>());
        }
        foreach (ParticleSystem p in wc.steamParticles) {
            if (p.gameObject.GetComponent<ParticleDragWithSpeed>()) return;
            inheritVelocityModule = p.inheritVelocity;
            inheritVelocityModule.enabled = true;

            //ParticleDragWithSpeed pd = p.gameObject.AddComponent<ParticleDragWithSpeed>();
            //pd.myRigidbody = wc.Rigidbody;
            //pd.waterCannon = wc;
            
            UnityEngine.Object.Destroy(p.gameObject.GetComponent<ParticleAddForce>());
        }
    }

    void SetupGravityWell(GameObject o, float radius, bool isMoon, float atmoFalloffStartRadius, float atmoFalloffAltitude) {
        ((List<object[]>)ObjectExplorer.Storage["AirDensity"][1]).Add(new object[] { o, radius, isMoon, atmoFalloffStartRadius, atmoFalloffAltitude });
    }
    
    // Token: 0x04000006 RID: 6
    public static bool LoadingSolarSystem = false;

    // Token: 0x04000008 RID: 8
    public static bool SolarSystemLoaded = false;

    // Token: 0x04000009 RID: 9
    public static GameObject PlanetSystem;

    // Token: 0x0400000A RID: 10
    public Vector3 grav;

    Vector3 worldCenter = new Vector3(0, -1650f * Scale, 0);

    ColorfulFog fogg = null;

    Gradient atmosphericColour = new Gradient();
}