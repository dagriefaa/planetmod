﻿using UnityEngine;
using spaar.ModLoader.Internal.Tools;
using System.Collections.Generic;
using Modding.Blocks;

namespace PlanetMod {

    public class Mapper : MonoBehaviour {

        void Awake() {
            List<ComponentEntry.FieldMapping> mappings = new List<ComponentEntry.FieldMapping>();
            
            mappings.Clear();
            mappings.Add(new ComponentEntry.FieldMapping("SunController.Rotation", typeof(bool), c => (c as SunController).Rotation, (c, x) => (c as SunController).Rotation = (bool)x));
            mappings.Add(new ComponentEntry.FieldMapping("SunController.RotationPeriod", typeof(float), c => (c as SunController).RotationPeriod, (c, x) => (c as SunController).RotationPeriod = (float)x));
            ComponentEntry.AddComponentMappings(typeof(SunController), "PlanetMod", mappings);

            Destroy(this);
        }
    }

}
