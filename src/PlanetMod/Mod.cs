using System;
using Modding;
using UnityEngine;

namespace PlanetMod
{
	public class Mod : ModEntryPoint
	{
		public override void OnLoad()
		{
            new GameObject("PlanetController").AddComponent<PlanetController>();
        }
	}
}
